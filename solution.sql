SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName from customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "STEVE";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000; 

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country IN ("USA","France","Canada");

SELECT firstName, lastName, city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE offices.officeCode = 5;

SELECT customerName FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

SELECT productName, customerName FROM customers 
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber 
	JOIN products ON orderdetails.productCode = products.productCode WHERE customerName = "Baane Mini Imports";

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM offices JOIN employees ON offices.officeCode = employees.officeCode JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE customers.country = offices.country;

SELECT products.productNAME, products.quantityInStock FROM productlines 
	JOIN products ON productlines.productLine = products.productLine
    WHERE products.productLine = "planes" AND products.quantityInStock <1000;

SELECT customers.customerName FROM customers WHERE phone LIKE "+81%";